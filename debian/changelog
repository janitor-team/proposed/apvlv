apvlv (0.4.0-2) unstable; urgency=medium

  * QA upload.
  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Tue, 05 Oct 2021 17:40:45 -0400

apvlv (0.3.0-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * debian/patches: Refresh patches.

 -- Boyuan Yang <byang@debian.org>  Fri, 03 Sep 2021 19:19:18 -0400

apvlv (0.2.0-2) unstable; urgency=medium

  * QA upload.
  * debian/control:
    + Bump Standards-Version to 4.5.1.
    + Bump debhelper compat to v13.
    + Use Debian GitLab for Vcs-* fields.

 -- Boyuan Yang <byang@debian.org>  Wed, 14 Apr 2021 10:31:23 -0400

apvlv (0.2.0-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
    - Fixes building with latest poppler. (Closes: #971102)

 -- Adrian Bunk <bunk@debian.org>  Mon, 26 Oct 2020 13:39:29 +0200

apvlv (0.1.5+dfsg-3) unstable; urgency=medium

  * QA upload.
  * Fix FTBFS with gcc-7. (Closes: #853317)
  * dh 10.

 -- Adam Borowski <kilobyte@angband.pl>  Wed, 30 Aug 2017 00:54:50 +0200

apvlv (0.1.5+dfsg-2) unstable; urgency=low

  * QA upload.
  * Disable APVLV_WITH_HTML to remove the dependency on the
    deprecated libwebkitgtk-3.0-0. (Closes: #866628)

 -- Adrian Bunk <bunk@debian.org>  Sun, 16 Jul 2017 20:26:05 +0300

apvlv (0.1.5+dfsg-1) unstable; urgency=medium

  * QA upload.
  * Set maintainer to QA team (see #798945).
  * New upstream release.  (Closes: #662806)
    - New build dependency: libpoppler-private-dev
    - Build dependency change libgtk2.0-dev -> libgtk-3-dev
    - Drop d/copyright paragraph for autoconf files that are no longer present.
    - Drop obsolete d/desktop.
      Upstream now provides a desktop file.
  * Enable build flags to view HTML and text files.
    - New build dependency: libwebkitgtk-3.0-dev
  * Modernise d/rules:
    - Use dh sequencer
    - Use --parallel
    - Add d/install
    - Append to d/dirs
    - Bump debhelper compat & dependency to 9
    - Enable all hardening
  * Convert d/copyright to DEP-5.
    - Correct upstream license GPL-2 -> GPL-2+.
      See the headers of the source files.
    - Add paragraph for icons, using the copyright file of src:gnome-icon-theme.
  * Update homepage, d/copyright & d/watch for upstream's move to GitHub
    (Closes: #651635, #738772).
    Thanks to Andreas Moog and Matanya Moses for reporting this.
  * Update upstream author's e-mail address in d/copyright.
  * Add myself to copyright paragraph for debian/ subdir.
  * Add gbp.conf to filter 15M of library source code and DLLs without
    source code that are needed only for the Windows build of apvlv.
    - Add Debian version mangle to d/watch.
  * Bump standards version to 3.9.8.
    - Drop obsolete d/menu.
  * Add upstream metadata file.
  * Add doc-base registration for Startup.pdf.
  * Switch to source format 3.0 (quilt).
  * Add 0001-manpage-spelling.patch
  * Add 0002-desktop-file-keywords.patch
  * Add 0003-add-Icon-to-desktop-file.patch
  * Add 0004-debian-paths.patch
  * Add 0005-fix-build-with-GCC-6.patch (Closes: #811839)
  * Add 0006-restore-Debian-CXXFLAGS.patch
  * Add Vcs-* fields.
  * Run wrap-and-sort -abst.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 27 Jun 2016 13:28:54 +0900

apvlv (0.1.1-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Add support for poppler >= 0.18. (Closes: #651851)

 -- Pino Toscano <pino@debian.org>  Tue, 20 Mar 2012 23:11:31 +0100

apvlv (0.1.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add support for poppler >= 0.15. Patch by Pino Toscano. (Closes: #627609)

 -- Michael Biebl <biebl@debian.org>  Wed, 01 Jun 2011 22:18:49 +0200

apvlv (0.1.1-1) unstable; urgency=low

  * Suggest poppler-data
  * Change CMAKE_INSTALL_PREFIX to "/usr"
  * Add cmake to build dependencies
  * Imported Upstream version 0.1.1 (Closes: #558893)
  * Fix debian/watch

 -- Lukas Gaertner <lukas.gaertner@credativ.de>  Mon, 14 Feb 2011 16:29:02 +0100

apvlv (0.0.9.8-1) unstable; urgency=low

  [Lukas Gaertner]
  * Adopt package (Closes: #607320)

  [Stefan Ritter]
  * Add libdjvulibre-dev to build dependencies (Closes: #602010)
  * Bump Standards-Version to 3.9.1
  * Imported Upstream version 0.0.9.8

 -- Lukas Gaertner <lukas.gaertner@credativ.de>  Thu, 23 Dec 2010 15:28:02 +0100

apvlv (0.0.9.7-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.0
  * Update debian/desktop, thanks to the gentoo maintainer of apvlv
  * Fix debian/watch

 -- Stefan Ritter <xeno@thehappy.de>  Sun, 04 Jul 2010 22:39:00 +0200

apvlv (0.0.9.6-1) unstable; urgency=low

  * New upstream release
  * Add debian/source/format (source 1.0)

 -- Stefan Ritter <xeno@thehappy.de>  Tue, 20 Apr 2010 13:53:00 +0200

apvlv (0.0.9.5-1) unstable; urgency=low

  * New upstream release (Closes: #576081, #576082)

 -- Stefan Ritter <xeno@thehappy.de>  Wed, 07 Apr 2010 11:13:08 +0200

apvlv (0.0.9.3-1) unstable; urgency=low

  * New upstream release
  * Bumped Standards-Version to 3.8.4, no changes needed

 -- Stefan Ritter <xeno@thehappy.de>  Thu, 04 Feb 2010 15:11:57 +0200

apvlv (0.0.9.2-1) unstable; urgency=low

  * New upstream release (Closes: #558893)

 -- Stefan Ritter <xeno@thehappy.de>  Wed, 20 Jan 2010 13:54:00 +0200

apvlv (0.0.9-1) unstable; urgency=low

  * New upstream release (Closes: #562495)
    Thanks to Daniel Friesel for the manpage

 -- Stefan Ritter <xeno@thehappy.de>  Thu, 07 Jan 2010 09:17:00 +0200

apvlv (0.0.8.1-1) unstable; urgency=low

  * New upstream release (Closes: #558833, #558894)

 -- Stefan Ritter <xeno@thehappy.de>  Fri, 11 Dec 2009 12:50:00 +0200

apvlv (0.0.8.0-1) unstable; urgency=low

  * New upstream release
  * Removed patchsystem and using a debhelper 5 compatible rules file
  * Removed encoding from desktop file since it is deprecated by the
    FreeDesktop standard
  * Removed apvlvrc.example from /usr/share/doc/apvlv/ since it is in
    /etc/apvlvrc

 -- Stefan Ritter <xeno@thehappy.de>  Wed, 09 Dec 2009 16:28:00 +0200

apvlv (0.0.7.5-1) unstable; urgency=low

  * New upstream release
  * Switched from dpatch to quilt

 -- Stefan Ritter <xeno@thehappy.de>  Thu, 19 Nov 2009 23:56:43 +0200

apvlv (0.0.7.4-1) unstable; urgency=low

  * New upstream release
  * Removed debian/conffiles

 -- Stefan Ritter <xeno@thehappy.de>  Wed, 07 Oct 2009 20:11:00 +0200

apvlv (0.0.7.3-1) unstable; urgency=low

  * New upstream release
  * Added apvlvrc to conffiles

 -- Stefan Ritter <xeno@thehappy.de>  Sun, 19 Sep 2009 19:31:00 +0200

apvlv (0.0.7.2-1) unstable; urgency=low

  * New upstream release
  * Improved the long description
  * Added patch that fixes a makefile path problem
    (02-fix-path-in-makefile.dpatch)

 -- Stefan Ritter <xeno@thehappy.de>  Wed, 09 Sep 2009 13:13:13 +0200

apvlv (0.0.6.11-1) unstable; urgency=low

  * New upstream release (Closes: #542231)
  * Added README.source
  * Improved the long description
  * Bumped Standards-Version to 3.8.3, no changes needed

 -- Stefan Ritter <xeno@thehappy.de>  Sun, 30 Aug 2009 19:29:02 +0200

apvlv (0.0.6.10-1) unstable; urgency=low

  * New upstream release

 -- Stefan Ritter <xeno@thehappy.de>  Wed, 29 Jul 2009 10:12:19 +0200

apvlv (0.0.6.8-1) unstable; urgency=low

  * Initial release (Closes: #537230)

 -- Stefan Ritter <xeno@thehappy.de>  Wed, 15 Jul 2009 14:48:33 +0200
